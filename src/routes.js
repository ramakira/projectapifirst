import { createStackNavigator, createAppContainer } from "react-navigation";

import FirstPage from "./pages/FirstPage.js";
import Detail from "./pages/Detail.js";

const AppNavigator = createStackNavigator({
  FirstPage: {
    screen: FirstPage
  },
  Detail: {
    screen: Detail
  }
});

export default createAppContainer(AppNavigator);
