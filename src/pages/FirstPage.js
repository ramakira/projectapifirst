import React, { Component } from "react";
import {
  ScrollView,
  View,
  Text,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  StyleSheet
} from "react-native";
import axios from "axios";

export default class FirstPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      news: []
    };
  }

  async componentDidMount() {
    try {
      const response = await axios.get(
        "http://doc.greatworks.id/api/latihan/news/list"
      );
      this.setState({ news: response.data.data });
    } catch (error) {
      console.error("Apps Error");
    }
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.text_banner}>Your Everyday</Text>
        <Text style={styles.text_banner}>Inspiration</Text>

        <View>
          <TouchableOpacity>
            <Image style={styles.banner} source={require("../img/1.png")} />
          </TouchableOpacity>
        </View>

        <View style={styles.text_position}>
          <Text style={styles.text_banner2}>People doesn't know</Text>
          <Text style={styles.text_banner2}>about us</Text>
          <Text style={styles.text_banner3}>Posted by 26 june 2019</Text>
        </View>

        {this.state.news.length > 0 && (
          <View style={styles.content}>
            {this.state.news.map((item, index) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("Detail", {
                    NewsID: item.id
                  })
                }
                style={styles.touchable}
              >
                <Image
                  style={styles.image_content}
                  source={{ uri: item.image }}
                />
                <Text key={index} style={styles.text_content}>
                  {item.title}
                </Text>
                <Text key={index} style={styles.text_content2}>
                  {item.post_date}
                </Text>
              </TouchableOpacity>
            ))}
          </View>
        )}
      </ScrollView>
    );
  }
}

FirstPage.navigationOptions = () => ({
  header: null
});

const styles = StyleSheet.create({
  container: {
    padding: 25
  },
  text_banner: {
    fontSize: 24,
    fontWeight: "bold",
    color: "black"
  },
  banner: {
    width: "100%",
    height: 170,
    overflow: "hidden",
    borderRadius: 10,
    marginTop: 15
  },
  text_position: {
    position: "absolute"
  },
  text_banner2: {
    fontSize: 20,
    fontWeight: "bold",
    color: "white",
    top: 165,
    left: 20
  },
  text_banner3: {
    fontSize: 14,
    color: "white",
    top: 165,
    left: 20
  },
  content: {
    flexDirection: "row",
    flexWrap: "wrap",
    padding: 0,
    marginVertical: 10,
    justifyContent: "space-between"
  },
  image_content: {
    borderRadius: 10,
    width: 145,
    height: 130
  },
  text_content: {
    color: "black",
    textAlign: "left",
    fontWeight: "bold"
  },
  text_content2: {
    marginBottom: 10
  },
  touchable: {
    alignItems: "center",
    width: "43%",
    margin: "2%"
  }
});
