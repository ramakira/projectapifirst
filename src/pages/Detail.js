import React, { Component } from "react";
import {
  ScrollView,
  Text,
  Image,
  View,
  ActivityIndicator,
  StyleSheet
} from "react-native";
import axios from "axios";
import { WebView } from "react-native-webview";

export default class Detail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      detail: {}
    };
  }

  async componentDidMount() {
    try {
      const response = await axios.get(
        "http://doc.greatworks.id/api/latihan/news/detail/" +
          this.props.navigation.state.params.NewsID
      );
      console.warn(response);
      this.setState({ detail: response.data.data });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        {this.state.detail.title && (
          <View>
            <Image
              style={styles.image}
              source={{ uri: this.state.detail.image }}
            />
            <Text style={styles.judul}>{this.state.detail.title}</Text>
            <Text style={styles.judul2}>
              Posted by {this.state.detail.post_date}
            </Text>
            <WebView
              style={styles.webview}
              scalesPageToFit={false}
              source={{
                html: this.state.detail.content
              }}
            />
          </View>
        )}
        {!this.state.detail.title && (
          <ActivityIndicator color="black" size="large" />
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 20
  },
  image: {
    width: "100%",
    height: 150
  },
  webview: {
    width: "100%",
    height: 400
  },
  judul: {
    fontSize: 18,
    fontWeight: "bold",
    color: "black"
  },
  judul2: {
    color: "black"
  }
});
